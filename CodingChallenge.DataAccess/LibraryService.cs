﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using CodingChallenge.DataAccess.Interfaces;
using CodingChallenge.DataAccess.Models;
using CodingChallenge.Utilities;

namespace CodingChallenge.DataAccess
{
    public class LibraryService : ILibraryService
    {
        public LibraryService() { }

        private IEnumerable<Movie> GetMovies()
        {
            return _movies ?? (
                _movies = ConfigurationManager.AppSettings["LibraryPath"].FromFileInExecutingDirectory().DeserializeFromXml<Library>().Movies);
        }

        /// <summary>
        /// This method is implemented to link the franchises with the movies
        /// </summary>
        private void UpdateWithFranchise()
        {
            if (_franchises == null)
            {
                _franchises = new Dictionary<string, List<string>>();
                string[] franchises = ConfigurationManager.AppSettings["FranchisesPath"].FromFileInExecutingDirectory().Split(new string[] { "\n\n\n" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string franchise in franchises)
                {
                    var moviesList = franchise.Split('\n');
                    _franchises.Add(moviesList[0], moviesList.Skip(2).ToList());
                }
                foreach(Movie movie  in _movies)
                {
                    movie.Franchise = GetFranchiseName(movie.Title);
                }
            }
        }

        private string GetFranchiseName(string title)
        {
            
            foreach (var franchise in _franchises)
            {
                if (franchise.Value.ToList().Contains(title))
                    return franchise.Key;
            }
            return string.Empty;
        }

        private IEnumerable<Movie> _movies { get; set; }
        private IDictionary<string,List<string>> _franchises { get; set; }
        public int SearchMoviesCount(string title)
        {
            return SearchMovies(title).Count();
        }

        public IEnumerable<Movie> SearchMoviesByRating(float rating,bool isAbove)
        {
            var movies = GetMovies();
            movies = isAbove ? movies.Where(movie => movie.Rating > rating) : movies.Where(movie => movie.Rating <= rating);
            return movies;
        }
        public IEnumerable<Movie> SearchMoviesByDateRange(int startYear, int endYear)
        {
            var movies = GetMovies().Where(movie => movie.Year >= startYear && movie.Year <= endYear); 
            return movies;
        }
        public IEnumerable<Movie> SearchMoviesByFranchise(string title)
        {
            var movies = GetMovies();
            UpdateWithFranchise();
            movies = movies.Where(movie => movie.Franchise.ToLower().IndexOf(title.ToLower()) > -1);
            return movies;
        }
        public IEnumerable<Movie> SearchMovies(string title, int? skip = null, int? take = null, string sortColumn = null, SortDirection sortDirection = SortDirection.Ascending)
        {
            var movies = GetMovies().Where(s => s.Title.ToLower().Contains(title.ToLower()));
            UpdateWithFranchise();

            //To remove duplicates
            movies = movies.GroupBy(movie => movie.Title).Select(movie => movie.First()).ToList(); 

            //To handle sorting functionality.
            if (sortColumn != null)
            {
                if(sortColumn == "Title")
                    movies = (sortDirection == SortDirection.Ascending) ? 
                        movies.OrderBy(movie =>
                        movie.Title.StartsWith("A ", StringComparison.OrdinalIgnoreCase) || movie.Title.StartsWith("An ", StringComparison.OrdinalIgnoreCase) || movie.Title.StartsWith("The ", StringComparison.OrdinalIgnoreCase) ?
                        movie.Title.Substring(movie.Title.IndexOf(" ") + 1) : movie.Title) :
                        movies.OrderByDescending(movie =>
                        movie.Title.StartsWith("A ", StringComparison.OrdinalIgnoreCase) || movie.Title.StartsWith("An ", StringComparison.OrdinalIgnoreCase) || movie.Title.StartsWith("The ", StringComparison.OrdinalIgnoreCase) ?
                        movie.Title.Substring(movie.Title.IndexOf(" ") + 1) : movie.Title)
                        ;
                else
                     movies = (sortDirection == SortDirection.Ascending) ?
                            movies.OrderBy(s => s.GetType().GetProperty(sortColumn).GetValue(s)) :
                            movies.OrderByDescending(s => s.GetType().GetProperty(sortColumn).GetValue(s));
                
            }
            if (skip.HasValue && take.HasValue)
            {
                movies = movies.Skip(skip.Value).Take(take.Value);
            }

            return movies.ToList();
        }
    }
}
