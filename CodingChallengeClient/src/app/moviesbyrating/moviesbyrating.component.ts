import { Component, OnInit } from '@angular/core';
import { MovieService } from '../movie.service';
import { IMovie } from '../movielibrary/movielibrary.component';

@Component({
  selector: 'app-moviesbyrating',
  templateUrl: './moviesbyrating.component.html',
  styleUrls: ['./moviesbyrating.component.css']
})
export class MoviesbyratingComponent implements OnInit {

  public data: IMovie[]=[];
  public pageData:IMovie[] = [] ;
  public errorMsgCD:string = "";
  movieRating:number = 0;
  isAbove:boolean = false;
  constructor(private movieService: MovieService) {

  }

  ngOnInit(): void {
  }

  getData() {
    this.movieService.getAllMoviesByRating(this.movieRating,this.isAbove).subscribe((data) => {
      this.data = data;
      this.pageData = this.data
      console.log(this.pageData);
    }, (err) => {
      this.errorMsgCD = err;
    })
  }

}
