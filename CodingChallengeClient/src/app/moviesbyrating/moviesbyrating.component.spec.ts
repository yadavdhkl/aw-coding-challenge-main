import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviesbyratingComponent } from './moviesbyrating.component';

describe('MoviesbyratingComponent', () => {
  let component: MoviesbyratingComponent;
  let fixture: ComponentFixture<MoviesbyratingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoviesbyratingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesbyratingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
