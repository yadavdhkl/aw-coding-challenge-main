import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviesbyfranchiseComponent } from './moviesbyfranchise.component';

describe('MoviesbyfranchiseComponent', () => {
  let component: MoviesbyfranchiseComponent;
  let fixture: ComponentFixture<MoviesbyfranchiseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoviesbyfranchiseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesbyfranchiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
