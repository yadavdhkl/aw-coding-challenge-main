import { Component, OnInit } from '@angular/core';
import { MovieService } from '../movie.service';
import { IMovie } from '../movielibrary/movielibrary.component';

@Component({
  selector: 'app-moviesbyfranchise',
  templateUrl: './moviesbyfranchise.component.html',
  styleUrls: ['./moviesbyfranchise.component.css']
})
export class MoviesbyfranchiseComponent implements OnInit {

  public data: IMovie[] =[];
  public pageData:IMovie[] = [] ;
  public errorMsgCD:string = "";
  constructor(private movieService: MovieService) { 
   
  }

  ngOnInit(): void {
    this.movieService.getAllMovies().subscribe((data) => {
      this.data = data;
      this.pageData = this.data
    }, (err) => {
      this.errorMsgCD = err;
    })
  }

  searchData() {
    var search = document.getElementById('mySearch') as HTMLInputElement;
    // @ts-ignore
    if(search.value == "")
    {
      this.pageData = this.data;
      return;
    }

    this.movieService.getAllMoviesByFranchise(search.value.toLocaleLowerCase()).subscribe((data) => {
      this.pageData = data
    }, (err) => {
      this.errorMsgCD = err;
    })
  }

}
