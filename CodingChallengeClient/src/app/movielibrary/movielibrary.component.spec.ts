import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MovielibraryComponent } from './movielibrary.component';

describe('MovielibraryComponent', () => {
  let component: MovielibraryComponent;
  let fixture: ComponentFixture<MovielibraryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MovielibraryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MovielibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
