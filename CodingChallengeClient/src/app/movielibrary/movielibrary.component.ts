import { Component, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-movielibrary',
  templateUrl: './movielibrary.component.html',
  styleUrls: ['./movielibrary.component.css']
})
export class MovielibraryComponent implements OnInit {


  // @ts-ignore
  public data: IMovie[];
  public pageData:IMovie[] = [] ;
  public errorMsgCD:string = "";
  constructor(private movieService: MovieService) { 
   
  }

  ngOnInit(): void {
    this.movieService.getAllMovies().subscribe((data) => {
      this.data = data;
      this.pageData = this.data
    }, (err) => {
      this.errorMsgCD = err;
    })
  }

  searchData() {
    var search = document.getElementById('mySearch') as HTMLInputElement;
    // @ts-ignore
    this.pageData = this.data.filter(item => item.Title.toLowerCase().indexOf(search.value.toLowerCase()) !== -1);
  }
   

}

export interface IMovie {
  ID:number,
  Title:string,
  Year:number,
  Rating:number
  Franchise:string
};
