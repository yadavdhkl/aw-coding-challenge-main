import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MovielibraryComponent } from './movielibrary/movielibrary.component';
import { MoviesbydateComponent } from './moviesbydate/moviesbydate.component';
import { MoviesbyfranchiseComponent } from './moviesbyfranchise/moviesbyfranchise.component';
import { MoviesbyratingComponent } from './moviesbyrating/moviesbyrating.component';

const routes: Routes = [
  {path:"",component:HomeComponent},
  {path:"movielibrary", component : MovielibraryComponent},
  {path:"moviesbyrating", component : MoviesbyratingComponent},
  {path:"moviesbyfranchise", component : MoviesbyfranchiseComponent},
  {path:"moviesbydaterange", component : MoviesbydateComponent}  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
