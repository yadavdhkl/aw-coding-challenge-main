export interface IMovie {
      id:int;
      title:string;
      year:int;
      rating:float;
      franchise:string;
};