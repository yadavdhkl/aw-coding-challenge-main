import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MovielibraryComponent } from './movielibrary/movielibrary.component';
import { SortDirective } from './directives/sort.directive';
import { HomeComponent } from './home/home.component';
import { TableComponent } from './table/table.component';
import { MoviesbyratingComponent } from './moviesbyrating/moviesbyrating.component';
import { FormsModule } from '@angular/forms';
import { MoviesbyfranchiseComponent } from './moviesbyfranchise/moviesbyfranchise.component';
import { MoviesbydateComponent } from './moviesbydate/moviesbydate.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    AppComponent,
    MovielibraryComponent,
    SortDirective,
    HomeComponent,
    TableComponent,
    MoviesbyratingComponent,
    MoviesbyfranchiseComponent,
    MoviesbydateComponent
      
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
