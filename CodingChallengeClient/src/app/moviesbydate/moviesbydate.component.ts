import { Component, OnInit } from '@angular/core';
import { MovieService } from '../movie.service';
import { IMovie } from '../movielibrary/movielibrary.component';

@Component({
  selector: 'app-moviesbydate',
  templateUrl: './moviesbydate.component.html',
  styleUrls: ['./moviesbydate.component.css']
})
export class MoviesbydateComponent implements OnInit {

  public data: IMovie[]=[];
  public pageData:IMovie[] = [] ;
  public errorMsgCD:string = "";
  startYear:number = 0;
  endYear:number=0;
  constructor(private movieService : MovieService) { }

  ngOnInit(): void {
  }

  getData() {
    this.errorMsgCD="";
    this.movieService.getAllMoviesByDateRange(this.startYear,this.endYear).subscribe((data) => {
      this.data = data;
      this.pageData = this.data
      console.log(this.pageData);
    }, (err) => {
      this.errorMsgCD = err;
    })
  }

}
