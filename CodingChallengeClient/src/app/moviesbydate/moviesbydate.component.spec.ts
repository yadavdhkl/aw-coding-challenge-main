import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviesbydateComponent } from './moviesbydate.component';

describe('MoviesbydateComponent', () => {
  let component: MoviesbydateComponent;
  let fixture: ComponentFixture<MoviesbydateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoviesbydateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesbydateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
