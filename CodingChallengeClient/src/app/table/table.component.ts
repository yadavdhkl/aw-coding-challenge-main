import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  p:number =1;
  @Input() pageData : Array<any> = [];
  constructor() { }

  ngOnInit(): void {
  }

}
