import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, retry} from "rxjs/operators";
import {environment} from "../environments/environment"

import {error} from "@angular/compiler/src/util";
import { IMovie } from './movielibrary/movielibrary.component';
@Injectable({
  providedIn: 'root'
})
export class MovieService {
  //for fetching the data
  constructor(private httpClient: HttpClient) { }

 /* get all customers
  which type of data come from the server is observable
  we have to tell which type of data will coming
  public getAllCustomers():Observable<any[]>
  thats why we have to create an interface*/

  public getAllMovies():Observable<IMovie[]>{
    let dataURL:string = `${environment.apiURL}/api/movieapi/movies`;
    //get will return something thats why we write any
    //pipe is do asynchronus calls to the url
    return this.httpClient.get<any>(dataURL).pipe(
      retry(1),
      catchError(this.handleErrors)
    )
  }

  public getAllMoviesByRating(rating:number,isAbove:boolean):Observable<IMovie[]>{
    let dataURL:string = `${environment.apiURL}/api/MovieApi/MoviesByRating?rating=${rating}&isAbove=${isAbove}`;
    //get will return something thats why we write any
    //pipe is do asynchronus calls to the url
    return this.httpClient.get<any>(dataURL).pipe(
      retry(1),
      catchError(this.handleErrors)
    )
  }

  public getAllMoviesByFranchise(search:string):Observable<IMovie[]>{
    let dataURL:string = `${environment.apiURL}/api/MovieApi/MoviesByFranchise?franchise=${search}`;
    //get will return something thats why we write any
    //pipe is do asynchronus calls to the url
    return this.httpClient.get<any>(dataURL).pipe(
      retry(1),
      catchError(this.handleErrors)
    )
  }

  public getAllMoviesByDateRange(startYear:number,endYear:number):Observable<IMovie[]>{
    let dataURL:string = `${environment.apiURL}/api/MovieApi/MoviesByDateRange?start=${startYear}&end=${endYear}`;
    //get will return something thats why we write any
    //pipe is do asynchronus calls to the url
    return this.httpClient.get<any>(dataURL).pipe(
      retry(1),
      catchError(this.handleErrors)
    )
  }

  

  //for error handler
  public handleErrors(getError:HttpErrorResponse){
    let errorMessage:string = "";
    if (getError.error instanceof ErrorEvent){
      //client side error
      errorMessage = getError.error.message;
    }
    else {
      //server error
      errorMessage = `Status: ${getError.status} Message: ${getError.message}`;
    }
    return throwError(errorMessage);
  }
  
}
