﻿using CodingChallenge.DataAccess;
using CodingChallenge.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CodingChallenge.WebAPI.Controllers
{
    public class MovieController : ApiController
    {
        public ILibraryService LibraryService { get; private set; }
        public MovieController()
        {
            LibraryService = new LibraryService();
        }
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }
    }
}
