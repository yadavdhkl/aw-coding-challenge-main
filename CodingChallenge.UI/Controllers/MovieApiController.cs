﻿using CodingChallenge.DataAccess;
using CodingChallenge.DataAccess.Interfaces;
using CodingChallenge.DataAccess.Models;
using CodingChallenge.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CodingChallenge.UI.Controllers
{
    public class MovieApiController : ApiController
    {
        public ILibraryService LibraryService
        {
            get;
            private set;
        }
        public MovieApiController() { }

        //// GET: api/MovieApi/Movies?title

        [HttpGet]
        public IEnumerable<Movie> Movies(string title = "")
        {
            LibraryService = new LibraryService();
            var model = LibraryService.SearchMovies(title ?? "").ToList();

            return (model);
        }

        //// GET: api/MovieApi/MoviesByRating/{rating}/{isAbove}
        [HttpGet]
        public IEnumerable<Movie> MoviesByRating(float rating = 0, bool isAbove = true)
        {
            LibraryService = new LibraryService();
            var model = LibraryService.SearchMoviesByRating(rating, isAbove);
            return (model);
        }

        //// GET: api/MovieApi/MoviesByDateRange?start&end
        [HttpGet]
        public HttpResponseMessage MoviesByDateRange(int? start, int? end)
        {
            if (!end.HasValue || !start.HasValue)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Request start and end dates required"); ;
            }
            LibraryService = new LibraryService();
            var model = LibraryService.SearchMoviesByDateRange(start.Value, end.Value);
            return Request.CreateResponse(HttpStatusCode.OK, model);
        }

        //// GET: api/MovieApi/MoviesByFranchise?franchise
        [HttpGet]
        public HttpResponseMessage MoviesByFranchise(string franchise)
        {
            if (franchise == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Request Franchise required"); ;
            }
            LibraryService = new LibraryService();
            var model = LibraryService.SearchMoviesByFranchise(franchise);
            return Request.CreateResponse(HttpStatusCode.OK, model);
        }

    }
}